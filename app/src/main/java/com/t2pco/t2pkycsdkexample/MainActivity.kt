package com.t2pco.t2pkycsdkexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.t2pco.t2pekycsdk.v3.T2PKycSdk
import com.t2pco.t2pekycsdk.v3.services.models.T2PKycSdkConfig
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btOpenSdk.setOnClickListener {
            openSdk()
        }
    }

    private fun openSdk() {
        val config = T2PKycSdkConfig().apply {
            kycUserRef = "0000000000" // kycUserRef is refer to user
            kycToken = "xxx" // kycToken is key for access SDK
            environment = "test" // For test server, default is production
        }

        T2PKycSdk.initializeSdk(config).startSdk(this){
            if(it.meta.responseCode != 600){
                // Error
                Toast.makeText(
                    applicationContext,
                    "(${it.meta.responseCode})${it.meta.responseMessage}",
                    Toast.LENGTH_SHORT
                ).show()
                return@startSdk
            }

            // Success
            Toast.makeText(
                applicationContext,
                "Success",
                Toast.LENGTH_SHORT
            ).show()

            // You get user information form T2PKycSdkResponse.Data class
            Log.d("T2PKycSdkResponse.Data", "${it.data}")
            // You can send kycServiceCode and kycSessionCode to your server
            // for check or retrieve user information from API SDK Server
        }
    }
}
